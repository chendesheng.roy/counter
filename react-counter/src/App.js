import React, { useState } from 'react';

// function Counter() {
//   const [counter, setCounter] = useState(0)

//   return (
//     <div>
//       <button onClick={() => setCounter(counter + 1)}>+</button>
//       {counter}
//       <button onClick={() => setCounter(counter - 1)}>-</button>
//     </div>
//   );
// }

function Button(props) {
  // eslint-disable-next-line jsx-a11y/anchor-has-content
  return <button {...props} />
}

class Counter extends React.Component {
  constructor(props) {
    super(props)
    this.state = { num: 0 }
  }

  render() {
    return (
      <span>
        <Button onClick={() => this.setState({ num: this.state.num + 1 })}>+</Button>
        {this.state.num}
        <Button onClick={() => this.setState({ num: this.state.num - 1 })}>-</Button>
      </span>
    );
  }
}

function App() {
  const [counters, setCounters] = useState([])
  return (<>
    {counters.map((counter) =>
    (<div key={counter.id}>
      <Counter />
      <Button onClick={() => {
        setCounters(counters.filter(({ id }) => id != counter.id))
      }}>Del</Button>
    </div>)
    )}
    <Button onClick={() => {
      setCounters(counters.concat([{ id: ((new Date).getTime()) }]))
    }
    }>More</Button>
  </>
  );
}

export default App;
